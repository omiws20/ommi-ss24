# Aufgabe 1 - Abgabe 1/5/24
# Write 2 python function to get the indices of the sorted elements of given lists and compare the speed
# using "tradional programming"
import random
import numpy as np
import time

list1 = [23, 104, 5, 190, 8, 7, -3]
list2 = []
# random generate 1000000 integers 
list3 = [random.randint(1,100) for _ in range(1, 1000000)]

# Sorting by keeping the original indeces
def sortList(l, printout=True):
    # check for null or empty
    if l == None or len(l) == 0:
        raise Exception("List is null or empty")
    # get the indeces of the unsorted array
    orgIndices1 = [(i, v) for i,v in enumerate(l)]
    # get the indeces of the sorted array for comparison
    orgIndices2 = [(x, l.index(x)) for x in sorted(l)]
    # sort the list with inplace sorting
    l.sort()
    # !!! Auch Tests hinzufügen
    # Compare both lists and output?
    if printout:
        for i,v in enumerate(orgIndices1):
            if orgIndices2[i] != v:
                print(f"new value at index {i}={orgIndices2[i][0]} (instead of {v[1]})")


# Sort the first list traditionally
try:
    startTime = time.time()
    sortList(list1)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

# Sort the second list traditionally
try:
    startTime = time.time()
    sortList(list2)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

# Sort the third list traditionally without ouput
try:
    startTime = time.time()
    sortList(list3, False)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

