# Aufgabe 1
# Write 2 python function to get the indices of the sorted elements of given lists and compare the speed
# using numpy
import random
import numpy as np
import time

list1 = [23, 104, 5, 190, 8, 7, -3]
list2 = []
# random generate 1000000 integers 
list3 = [random.randint(1,100) for _ in range(1, 1000000)]

# Sorting by keeping the original indeces
def sortList(l, printout=True):
    # check for null or empty
    if l == None or len(l) == 0:
        raise Exception("List is null or empty")
    oldIndices = [(i, v) for i,v in enumerate(l)]
    # get the indeces that would sort the list
    newIndices = np.argsort(l)
    # sort the list
    sortedList = np.sort(l)
    # Compare both lists
    if printout:
        for t in oldIndices:
            if t[0] != newIndices[t[0]]:
                print(f"new value at index {t[0]}={sortedList[t[0]]} (instead of {l[t[0]]})")


# Sort the first list traditionally
try:
    startTime = time.time()
    sortList(list1)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

# Sort the second list traditionally
try:
    startTime = time.time()
    sortList(list2)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

# Sort the third list traditionally without ouput
try:
    startTime = time.time()
    sortList(list3, False)
    endTime = time.time()
    print(f"*** sorting took {(endTime-startTime):.4f}s")
except:
    print("!!! Unkown error !!!")

