# Erstellt 11/06/24

# 2 Würfel - einer mit 6 Seiten, einer mit 12 Seiten
# Wenn eine 5 gezogen wird, wie groß ist die Wahrscheinlichkeit, dass sie vom ersten Würfel stammt?
# Muss über eine Kombination aus totaler Wahrscheinlichkeit und dem Satz von Bayes berechnet werden

# Satz von Bayes
# P(A|B) = P(A und B) / P(B)
# P(A und B) = P(A|B) * P(B)

# Totale Wahrscheinlichkeit
# Ptotal = P(A|B) * P(B) + P(A|BNeg) * P(BNeg)

# Wahrscheinlichkeit dafür, dass die 5 vom ersten Würfel stammt
pA = 0
# Wahrscheinlichkeit dafür, dass eine 5 gewürfelt wird
pB = 0

pW = 1/6 # Wahrscheinlichkeit, dass mit dem ersten Würfel eine 5 gewürfelt wird
pBANeg = 1/12 # Wahrscheinlichkeit, dass die 5 vom zweiten Würfel stammt
pA = 1/2

# Berechnen der totalen Wahrscheinlichkeit für eine 5 über den Baumpfad
pB = pA * pW + pBANeg * pA
print("P(5) =", pB)


# Jetzt muss pA durch Umstellen des Satzes von Bayes berechnet werden
# Gesucht ist P(A|B) dh. die Wahrscheinlichkeit, dass die 5 vom ersten Würfel stammt, wenn eine 5 gewürfelt wurde
# P(A|B) = P(A und B) / P(B)

pAB = pW * pA / pB

print("P(5|6) =", pAB)