# Lösung des klassischen Ziegenproblems
# Erstellt: 10/06/2024

import random

# Es geht um abhängige Wahrscheinlichkeiten und den Satz von Bayes

# Die Wahrscheinlichkeit, dass die Ziege hinter Tür 1 ist, beträgt 1/3
# Die Wahrscheinlichkeit, dass die Ziege hinter Tür 2 ist, beträgt 1/3
# Die Wahrscheinlichkeit, dass die Ziege hinter Tür 3 ist, beträgt 1/3

pA = 1/3
pGA = 0
pGNotA = 1

def DieZiegenShow(Anzahl, Wechsel = True):
    pGA = 0
    for i in range(1, Anzahl + 1):
        # Zufällige Wahl des Autos
        auto = random.randint(1, 3)
        # Zufällige Wahl des Kandidaten
        kandidat = random.randint(1, 3)
        # print(f"Die Kandidatin wählt Tür Nr. {kandidat}")
        # Der Moderator wählt eine Tür mit einer Ziege
        while True:
            moderator = random.randint(1, 3)
            if moderator != kandidat and moderator != auto:
                break
        # print(f"Der Moderator öffnet Tür {moderator} mit einer Ziege")
        # Der Kandidat wechselt die Tür?
        if Wechsel:
            kandidat = [_ for _ in [1,2,3] if _ != moderator and _ != kandidat][0]
        if kandidat == auto:
            # Wahrscheinlichkeit, dass der Kandidat das Auto gewinnt hängt davon ab, ob er die Tür gewechselt hat
            pGA += 2/3 if Wechsel else 1/3
            print(f"*** {i:000}: Die Kandidatin hat das Auto gewonnen")
    return pGA / Anzahl


# Anzahl Durchläufe
n = 100
print(f"Die Wahrscheinlichkeit, dass der Kandidat das Auto gewinnt, wenn er die Tür wechselt, beträgt {DieZiegenShow(n, True):.0%}")

print(f"Die Wahrscheinlichkeit, dass der Kandidat das Auto gewinnt, wenn er die Tür nicht wechselt, beträgt {DieZiegenShow(n, False):.0%}")