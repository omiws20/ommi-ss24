# Erstellt 11/06/24

# Wahrscheinlichkeiten für den sehr unfairen Würfel berechnen
probabilities = [pow(2,n) / 63 for n in range(0, 6)]

# probabilities = [1/6, 1/6, 1/6, 1/6, 1/6, 1/6]

# Erwartungswert berechnen
resultspace = [_ for _ in range(1, 7)]
expected_value = sum([p * r for p, r in zip(probabilities, resultspace)])
print("Expected Value:", expected_value)

# Ohne numpy etwas umständlicher
variance = sum([(r - expected_value) ** 2 * p for r,p in zip(resultspace, probabilities)])
print("Variance:", variance)

# Ab hier alles mit numpy
import numpy as np

# Erwartungswert berechnen
expected_value = np.sum(resultspace * np.array(probabilities))
print("Expected Value (numpy):", expected_value)

# Durchschnitt berechnen
mean = np.mean(probabilities)

# Standardabweichung berechnen
squared_diffs = [(p - mean) ** 2 for p in probabilities]
print("Standard Deviation (numpy):", np.sqrt(np.mean(squared_diffs)))

# Varianz berechnen mit numpy
variance = variance = np.sum(((resultspace - expected_value) ** 2) * probabilities)
print("Variance (numpy):", variance)

