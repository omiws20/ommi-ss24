# Erstellt: 20/06/24
# Merge the two files to include the columns: Patient_ID, Sex, Age, BMI, Smoking_status, Number_of_visits, Disease and age group
# Participant_ID, Sex, Age, Smoking_status, BMI, Disease AgeGroup

import pandas as pd
from os import path

# Step 1: Read the patient CSV files into seperate DataFrames
csvPath1 = path.join(path.dirname(__file__), "PatientInfo.csv")
df1 = pd.read_csv(csvPath1)

csvPath2 = path.join(path.dirname(__file__), "Date.csv")
df2 = pd.read_csv(csvPath2)

# Step 2: Merge the two DataFrames on the values of the Participant_ID column into a new DataFrame
df3 = pd.merge(df1, df2, on="Participant_ID", how="inner")

# Step 3: Remove column VisitDate from the merged DataFrame
df3.drop(columns=["VisitDate"], inplace=True)

# Step 4: Define the age groups
bins = [40, 50, 60, 70, 120]
labels = ["40-49", "50-59", "60-69", "70+"]

# Step 5: Add a new column "AgeGroup" to the merged DataFrame
df3["AgeGroup"] = pd.cut(df3["Age"], bins=bins, labels=labels, right=False)

# Step 6: Display the merged DataFrame
print(df3.head(20))    

# Step 7: Save the merged DataFrame to a new CSV file
outputPath = path.join(path.dirname(__file__), "MergedData.csv")
df3.to_csv(outputPath, index=False)

