# Erstellt: 20/06/24
# Create a summary table and/or chart showing the number of visits for each month (irrespective of the year)

import datetime
import pandas as pd
from os import path

# Step 1: Read the patient CSV file into a DataFrame
csvPath = path.join(path.dirname(__file__), "Date.csv")
df = pd.read_csv(csvPath)

# Step 2: Check if VisitDate column is in datetime format
df["VisitDate"] = pd.to_datetime(df["VisitDate"],dayfirst=True, errors="coerce")

# Step 3: Drop rows with missing VisitDate
df.dropna(subset=["VisitDate"], inplace=True)

# Step 4: Extract the month from the VisitDate
df["Month"] = df["VisitDate"].dt.month.astype(int)

# Step 5: Group by month and count the number of visits
monthlyVisits = df.groupby("Month").size().reset_index(name="Number of Visits")

print(monthlyVisits)

# Step 6: Create a bar chart to visualize the data
import matplotlib.pyplot as plt

# Plot the number of visits for each month using a bar chart    
plt.figure(figsize=(10, 6))
colors = ["red", "green", "blue", "cyan", "magenta", "yellow", "black", "orange", "purple", "pink", "brown", "gray"]
plt.bar(monthlyVisits["Month"], monthlyVisits["Number of Visits"], color=colors)
plt.xlabel("Month")
plt.ylabel("Number of patient visits")
plt.title("Number of patient visits for each month")
plt.xticks(monthlyVisits["Month"], ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"])
plt.grid(axis="y", linestyle="--", alpha=0.7)
plt.show()