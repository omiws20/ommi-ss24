# Erstellt: 20/06/24
# Analyze the relationship between smoking status and the number of hospital visits, separately for the disease and healthy groups, and for the overall population

import pandas as pd
from os import path

# Step 1: Load the patient and visit info data into two dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
dfPatients = pd.read_csv(csvPath)
csvPath = path.join(path.dirname(__file__), "Date.csv")
dfVisits = pd.read_csv(csvPath)

# Step 2: Convert the VisitDate column 
dfVisits["VisitDate"] = pd.to_datetime(dfVisits["VisitDate"], dayfirst=True, errors="coerce")

# Step 3: Group by Participant_ID and count the number of visits
dfVisitCounts = dfVisits.groupby("Participant_ID").size().reset_index(name="VisitCount")
print(dfVisitCounts.head(20))

# Step 4: Merge the visit counts with the visit data
dfPatientVisits = pd.merge(dfPatients, dfVisitCounts, on="Participant_ID")

print(dfPatientVisits.head(20))

# Step 5: Group by smoking status and calculate the mean number of visits
dfMeanVisits = dfPatientVisits.groupby("Smoking_status")["VisitCount"].mean()
print(dfMeanVisits.head(20))

# The number of hospital visits for each smoking status is nearly the same for each group
# I don't see a significant relationship between smoking status and the number of hospital visits
# I don't what general population means in this context? Is it the combination of the disease and healthy groups?
# I would suggest to clarify the definition of the general population