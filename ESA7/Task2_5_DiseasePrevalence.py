# Erstellt: 20/06/24
# Calculate the disease prevalence (number of sick  / total number population) across different age groups in our dataset,
# and test if the prevalence is different across the group

import pandas as pd
import numpy as np
from os import path

# Step 1: Load the data into a dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
dfPatients = pd.read_csv(csvPath)

# Step 2: Define the age groups and add a new column to the DataFrame
ageNumbers = [40, 50, 60, 70, 100] 
labels = ["40-49", "50-59", "60-69", "70+"]

# Create a new column "Age Group" based on the categorial age column and the defined age group values
dfPatients["Age Group"] = pd.cut(dfPatients["Age"], bins=ageNumbers, labels=labels, right=False)

# Step 3: Calculate the disease prevalence across different age groups
ageGroupCounts = dfPatients["Age Group"].value_counts()
ageGroupSickCounts = dfPatients[dfPatients["Disease"] == 1]["Age Group"].value_counts()

ageGroupPrevalence = ageGroupSickCounts / ageGroupCounts

df_prevalence = pd.DataFrame({"Group count": ageGroupCounts, "Sick count": ageGroupSickCounts, 'Prevalence': ageGroupPrevalence})
print(df_prevalence)

# The prevalence is nearly the same of each age group

# Step 4: Test if the prevalence is different across the group
# using a chi-squared test to determine if there is a significant association between age group and disease status

import scipy.stats as stats

# Define a contingency table for the age group and disease columns
contingencyTable = pd.crosstab(dfPatients["Age Group"], dfPatients["Disease"])

# Perform a chi-squared test with the contingency table
chi2Value, pValue, freedomDegrees, expectedFrequencies = stats.chi2_contingency(contingencyTable)

# Print the results
print(f"Chi-squared: {chi2Value}")
print(f"P-value: {pValue}")
print(f"Degrees of Freedom: {freedomDegrees}")
print("Expected frequencies:")
print(expectedFrequencies)

# Since the p-value is greater than 0.05, we fail to reject the null hypothesis
# and conclude that there is no significant association between age group and disease status in the dataset

# The disease prevalence is nearly the same across different age groups, and there is no significant association between age group and disease status