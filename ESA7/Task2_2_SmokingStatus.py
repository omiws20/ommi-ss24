# Erstellt: 20/06/24
# Examine if the proportion of smoking status differs between the healthy and control groups
# ??? control group = disease group ???


import pandas as pd
import matplotlib.pyplot as plt
from os import path

# Step 1: Load the data into a dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
df = pd.read_csv(csvPath)

# Step 2: Create a contingency table for the smoking status and disease columns
# both are categorical variables
# 1 = disease, 2 = healthy
# margins=True adds the row and column totals
dfcontingency = pd.crosstab(df["Smoking_status"], df["Disease"], normalize="index")
print(dfcontingency)

# Plotting the proportions
dfcontingency.plot(kind='bar', stacked=True)
plt.title("Proportion of participants with diseases by their smoking status")
plt.xticks(rotation=0)
plt.xlabel("Smoking Status")
plt.ylabel("Health Status")
plt.legend(title="1 = Disease, 2 = Healthy")
plt.show()

# Except for the small group of smoking status "Prefer not to answer", the proportion of participants with diseases is the the same than the proportion of healthy participants
# This indicates that smoking status is not significantly associated with the disease status

