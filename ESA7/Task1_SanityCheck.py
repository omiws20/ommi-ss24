# Erstellt: 20/06/24
# Replace missing BMI values with the median BMI for the corresponding gender
import datetime
import pandas as pd
from os import path

# Step 1: Read the patient CSV file into a DataFrame
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
df = pd.read_csv(csvPath)

# Step 2: Define some variables for validation
sex_values = ["Male", "Female"] # assume only two gender to keep it simple
smoking_status_values = ["Previous", "Current", "Never", "Prefer not to answer"]
age_values = range(50, 110)
bmi_values = range(15, 60)

# Step 4: Define a function to check if a row passes validation
def validate_patientrow(row):
    if pd.isnull(row['Participant_ID']):
        return "No Participant_ID"
    if row["Sex"] not in sex_values:
        return "No valid sex value"
    if not isinstance(row["Age"], (int)) and (row["Age"] >= age_values[0] and row["Age"] <= age_values[1]):
        return "No age or not in valid range"
    if not isinstance(row["BMI"], (int, float)) or not bmi_values[0] <= row["BMI"] and row["BMI"] <= bmi_values[1]:
        return "No BMI or not in valid range"
    if row["Smoking_status"] not in smoking_status_values:
        return "No smoking status"
    if not isinstance(row["Disease"], (int)) and (row["Disease"] == 1 or row ["Disease"] == 0):
        return "No Disease or valid value"
    return "OK"

invalidRowCounter = 0
# Step 4: Validate each row
for index, row in df.iterrows():
  result = validate_patientrow(row)
  if result != "OK":
        print(f"Row {index} is invalid due to {result}")
        df.drop(index, inplace=True)
        invalidRowCounter += 1

print(f"Number of invalid rows: {invalidRowCounter}")

# Step 5: Repeat with second csv file
csvPath = path.join(path.dirname(__file__), "Date.csv")
df = pd.read_csv(csvPath)

# Step 6: Check if VisitDate column is in datetime format and use the day-month-year format
df["VisitDate"] = pd.to_datetime(df["VisitDate"], dayfirst=True, errors="coerce").dt.date

# drop all Nan values
df.dropna()

def validateDate(date):
    try:
        datetime.datetime.strptime(date, "%Y-%d-%M")
        return True
    except ValueError:
        return False
    
# Step 6: Validate each row
invalidRowCounter = 0
for index, row in df.iterrows():
    if not validateDate(str(row["VisitDate"])):
        print(f"Row {index} is invalid")
        df.drop(index, inplace=True)
        invalidRowCounter += 1

print(f"Number of invalid rows: {invalidRowCounter}")

