# Erstellt 20/06/24
# Compare the BMI between the healthy and control groups using a box or violin plot
# ??? control group = disease group ???

import pandas as pd
import seaborn as sbn
import matplotlib.pyplot as plt
from os import path

# Step 1: Load the data into a dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
df = pd.read_csv(csvPath)

# Step 2: Check for missing values in the BMI and Disease columns (optional)
print(df[["BMI", "Disease"]].isnull().sum())

# Step 3: Drop rows with missing values in the BMI and Disease columns
df.dropna(subset=["BMI", "Disease"], inplace=True)

# Step 4: Compare the BMI between the healthy and control groups with a box plot
sbn.boxplot(x="Disease", y="BMI", data=df)
plt.title("BMI comparison between healthy and control group participants")
plt.show()

# Step 5: Compare the BMI between the healthy and control groups with a violin plot
sbn.violinplot(x="Disease", y="BMI", data=df)
plt.title("BMI comparison between healthy and control group participants")
plt.xlabel("Disease Status")
plt.ylabel("BMI")
plt.show()
