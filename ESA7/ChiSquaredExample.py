# Perform a chi-squared test withe the contingency table
# to determine if the smoking status is associated with the disease status
# The null hypothesis is that the two variables are independent
# The alternative hypothesis is that the two variables are dependent
# The test returns the chi-squared value, p-value, degrees of freedom, and expected frequencies
# The p-value indicates the probability of observing the data if the null hypothesis is true
# If the p-value is less than the significance level (e.g., 0.05), we reject the null hypothesis
# and conclude that the variables are associated

import pandas as pd
import matplotlib.pyplot as plt
from os import path

# Step 1: Load the data into a dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
df = pd.read_csv(csvPath)

# Step 2: Create a contingency table for the smoking status and disease columns
# both are categorical variables
# 1 = disease, 2 = healthy
# margins=True adds the row and column totals
dfcontingency = pd.crosstab(df["Smoking_status"], df["Disease"], normalize="index")
print(dfcontingency)

import scipy.stats as stats
chi2Value, pValue, freedomDegrees, expectedFrequencies = stats.chi2_contingency(dfcontingency)

# Print the results
print(f"Chi-squared: {chi2Value}")
print(f"P-value: {pValue}")
print(f"Degrees of Freedom: {freedomDegrees}")
print("Expected frequencies:")
print(expectedFrequencies)

# Because the p-value is greater than 0.05, we fail to reject the null hypothesis 
# and conclude that smoking status is not significantly associated with the disease status