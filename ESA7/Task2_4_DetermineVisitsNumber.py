# Erstellt: 20/06/24
# Determine if the number of visits is higher in the disease group compared to the healthy group

import pandas as pd
from os import path

# Step 1: Load the patient and visit info data into two dataframe
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
dfPatients = pd.read_csv(csvPath)
csvPath = path.join(path.dirname(__file__), "Date.csv")
dfVisits = pd.read_csv(csvPath)

# Step 2: Convert the VisitDate column
dfVisits["VisitDate"] = pd.to_datetime(dfVisits["VisitDate"], dayfirst=True, errors="coerce")

# Step 3: Group by Participant_ID and count the number of visits
dfVisitCounts = dfVisits.groupby("Participant_ID").size().reset_index(name="VisitCount")

# Step 3: Merge the DataFrames on Participant_ID
dfMerged = pd.merge(dfPatients, dfVisitCounts, on="Participant_ID", how="inner")

# Step 4: Calculate the mean number of hospital visits for each group (disease and healthy)
dfMeanVisits = dfMerged.groupby("Disease")["VisitCount"].mean()

# Step 5: Display the mean number of visits for each group
diseaseVisits = dfMerged[dfMerged["Disease"] == 1]["VisitCount"]
healthyVisits = dfMerged[dfMerged["Disease"] == 2]["VisitCount"]

print(f"Mean of disease group visits: {diseaseVisits.mean()}")
print(f"Mean of health group visits: {healthyVisits.mean()}")

# the mean of both groups is nearly the same
# I don't see a significant difference in the number of visits between the disease and healthy groups

# # Step 6: Perform a t-test to compare the means
from scipy.stats import ttest_ind
t_stat, p_value = ttest_ind(diseaseVisits, healthyVisits)

print("\nT-test Results:")
print(f"T-statistic: {t_stat}")
print(f"P-value: {p_value}")

# a high p-value indicates that there is no significant difference in the number of visits between the two groups