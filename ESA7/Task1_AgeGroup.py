# Erstellt: 20/06/24
# Add a new column "age group " to split the age to 5 categories:  40-49, 50-59, 60-69, and above 70

import pandas as pd
from os import path

# Step 1: Read the patient CSV file into a DataFrame
csvPath = path.join(path.dirname(__file__), "PatientInfo.csv")
df = pd.read_csv(csvPath)

# Step 2: Define the age groups
bins = [40, 50, 60, 70, 120]
labels = ["40-49", "50-59", "60-69", "70+"]
df["AgeGroup"] = pd.cut(df["Age"], bins=bins, labels=labels, right=False)

print(df.head(20))
