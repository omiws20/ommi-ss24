# Erstellt: 20/06/24
# Calculate the number of visits each patient made since 1/1/2020
# If the VisitDate is missing, it indicates no visits during this period

import datetime
import pandas as pd
from os import path

# Step 1: Read the patient CSV file into a DataFrame
csvPath = path.join(path.dirname(__file__), "Date.csv")
df = pd.read_csv(csvPath)

# Step 2: Check if VisitDate column is in datetime format and use the day-month-year format
df["VisitDate"] = pd.to_datetime(df["VisitDate"], dayfirst=True, errors="coerce")

# Step 3: Filter out rows with a VisitDate before 1/1/2020
df = df[df["VisitDate"] >= datetime.datetime(2020, 1, 1)]

# Step 4: Group by Participant_ID and count the number of visits
df["Visits"] = df.groupby("Participant_ID")["VisitDate"].transform("count")

print(df.head(20))