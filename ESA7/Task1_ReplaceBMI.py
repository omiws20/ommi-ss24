# Erstellt: 20/06/24
# Replace missing BMI values with the median BMI for the corresponding gender
import pandas as pd
from os import path

# Step 1: Read the CSV file into a DataFrame
csvPath = path.join(path.dirname(__file__), 'PatientInfo.csv')

df = pd.read_csv(csvPath)

# Step 2: Print out the number of all rows with missing BMI values
missingBmiCount = df["BMI"].isnull().sum()
print(f"Number of missing BMI values: {missingBmiCount}")

# Step 3: Get the first male and female Participant_ID with missing BMI
condition = (df["BMI"].isna()) & (df["Sex"] == "Male")
missingBmiParticipantIdMale = df.loc[df[condition].first_valid_index()]["Participant_ID"] 
condition = (df["BMI"].isna()) & (df["Sex"] == "Female")
missingBmiParticipantIdFemale = df.loc[df[condition].first_valid_index()]["Participant_ID"] 

# Step 4: Calculate the median BMI for each gender
medianBmiBySex = df.groupby("Sex")["BMI"].median()
print(medianBmiBySex)

# Step 5: Replace missing BMI values with the corresponding median values
df["BMI"] = df.apply(
    lambda row: medianBmiBySex[row["Sex"]] if pd.isnull(row["BMI"]) else row["BMI"],
    axis=1
)

# Step 6: Print out the number of all rows with missing BMI values
missingBmiCount = df["BMI"].isnull().sum()
print(f"Number of missing BMI values: {missingBmiCount}")

# Step 7: Check the first Participant_ID with missing BMI
missingBmiMale = df[df["Participant_ID"] == missingBmiParticipantIdMale].iloc[0]
missingBmiFemale = df[df["Participant_ID"] == missingBmiParticipantIdFemale].iloc[0]
print(missingBmiMale)
print(missingBmiFemale)